# Cours 1 : Switching

## Sommaire

* [Switches behaviour](#switches-behaviour)
* [STP](#stp)
  * [Concept de boucle topologique](#concept-de-boucle-topologique)
  * [STP principles](#stp-principles)
  * [Overview of STP behaviour](#overview-of-stp-behaviour)
    * [BPDU, BID](#bpdu-et-bid)
    * [Fonctionnement de STP](#fonctionnement-de-stp)
    * [STP algorithms](#stp-algorithms)
* [VLAN](#vlan)
  * [Besoin](#besoin)
  * [Concept](#concept)
  * [Cisco : port *access* et *trunk*](#cisco-ports-access-et-trunk)
  * [VLAN natif](#vlan-natif)
  * [Bonnes pratiques](#bonnes-pratiques)
* [LACP](#lacp)

## Switches behaviour

* un switch est un équipement L2 (il n'utilise pas et ne comprend pas le protocole IP) : il utilise principalement [le protocole Ethernet](/memo/lexique.md#ethernet)
* équipement L2 donc ses interfaces possèdent une [adresse MAC](/memo/lexique.md#mac-media-access-control)
* un switch possède ***une table d'adresses MAC*** (*mac address table*) dans laquelle il stocke les MACs de tous les équipements du réseau qu'il connaît = tous les équipements sur le même segment réseau que lui
  * il associe à chacune des MACs l'interface qui permet de la joindre 
  * dans l'exemple suivant, les deux switches pourront avoir dans leur *mac adress table*
    * les 4 PCs
    * pour chacun d'entre eux l'autre switch
```
             +---+         +---+
             |PC3|         |PC4|
             +-+-+         +-+-+
               |             |
+---+      +---+---+     +---+---+
|PC2+------+  SW1  +-----+  SW2  |
+---+      +---+---+     +-------+
               |
             +-+-+
             |PC1|
             +---+
```

* un switch apprend au fur et à mesure quelle machine il peut joindre sur quel port
* lorsqu'un switch reçoit un [trame Ethernet](/memo/lexique.md#ethernet) avec une destination inconnue, il la renvoie sur tous ses ports réseau
  * ainsi le message sera forcément délivré (si la machine de destination est dispo)
  * le switch pourra apprendre lequel de ses ports à été utilisé pour joindre cette destination préalablement inconnue
* lorsqu'un switch reçoit une [trame en broadcast](/memo/lexique.md#d-adresse-mac-de-broadcast), il la renvoie simplement sur tous ses ports actifs (sauf celui d'où vient la trame)

---

Exemple : 
* considérons la même topologie qu'au dessus : 
```
             +---+         +---+
             |PC3|         |PC4|
             +-+-+         +-+-+
               |             |
+---+      +---+---+     +---+---+
|PC2+------+  SW1  +-----+  SW2  |
+---+      +---+---+     +-------+
               |
             +-+-+
             |PC1|
             +---+
```
1. admettons que les PCs connaissent les [adresses MAC](/memo/lexique.md#mac-media-access-control) (après échanges [ARP](/memo/lexique.md#arp-adress-resolution-protocol)) des autres PCs du réseau
2. admettons que la *mac address table* de SW1 est donc vide
3. PC2 effectue un ping vers PC3
4. SW1 va recevoir une trame avec pour destination la MAC de PC3
5. Ne sachant pas comment joindre PC3, SW1 va envoyer cette trames sur tous ses autres ports actifs : 
  * vers PC1
  * vers PC3
  * vers SW2
6. SW2 va faire de même : envoyer le message tous ses autres ports, et donc l'envoyer à PC4
7. Seule PC3 répondra à cette trame car le message lui est destiné
8. PC3 répond. SW1 va pouvoir apprendre sur quel port joindre PC3, et ajouter cette information dans sa *mac address table*

> **La *mac address table* est utilisée à chaque message traité par le switch pour qu'il soit en mesure de savoir sur quelle interface il doit envoyer une trame donnée afin qu'elle arrive jusqu'à son destinataire.**

## STP

Pour comprendre la nécessité d'utiliser STP, il faut s'intéresser aux problèmes de boucle réseau ou *boucle topologique*.

### Concept de boucle topologique

**Une boucle topologique au niveau L2 se produit lorsque, pour un switch, il existe plusieurs chemin pour aller à une destination donnée.**

Dans l'exemple qui suit, par exemple, SW1 peut joindre SW3 de deux façons différentes :
* en direct
* en passant par SW2

```
        +-------+
    +---+  SW2  +----+
    |   +-------+    |
    |                |
    |                |
+---+---+        +---+---+    +---+
|  SW1  +--------+  SW3  +----+PC1|
+-------+        +-------+    +---+

```

Les boucles tpoologiques amènent plusieurs problèmes : 
* risque de *broadcast storm*
  * un message en broadcast envoyé à l'un des switches sera renvoyé sur les deux autres, et ceci indéfiniment
  * le nombre de messages est en constante augmentation pendant un *broadcast storm*, les équipements finissent par ne plus fonctionner correctement du fait d'un trop grand nombre de trames à traiter
* des *mac address tables* instables
  * vu qu'il existe plusieurs chemins possibles pour joindre PC1, la table de SW1 sera constamment mise à jour 
  * l'adresse de PC1 sera joignable en passant par une interface donnée, puis par l'autre, puis par la première de nouveau, puis la deuxième, etc.

Une façon simple d'enrayer la création de boucle topologique est tout simplement de **fermer les ports de certains switches**.  
OK, mais quand ? Quel switch ? Quel port ? Comment réagir à un changement de topologie ?

**Welcome to STP.**

### STP principles

STP (pour *Spanning Tree Protocol*) :
* **est un protocole utilisé pour éviter les boucles topologiques réseau**
* tourne sur TOUS les switches concernés
* envoie des messages sur toutes les interfaces actives des switches concernés, en permanence, avec une fréquence de l'ordre de quelques secondes
* crée un consensus entre tous les switches : tous les switches se mettent d'accord sur un comportement à adopter
* **évite les [boucles](#concept-de-boucle-topologique) en désactivant automatiquement certains ports de certains switches**

### Overview of STP behaviour

#### BPDU et BID

Au sein d'une topologie STP, les switches sont identifiés par leur *BID*. Un *BID* est la concaténation de la priorité STP du switch et de sa [MAC address](/memo/lexique.md#mac-media-access-control).
* chaque switch a une priorité
* la plus basse priorité l'emporte
* les priorités sont des multiples de 4096
* un switch donné avec une priorité de `32768` et une interface avec une [MAC](/memo/lexique.md#mac-media-access-control) de `78:78:78:78:78:78` aura donc pour *BID* `32768787878787878` sur cette interface

Les messages envoyés sur tous les ports de tous les switches sont appelés *BPDU* (*Bridge Protocol Data Unit*). Les *BPDU* de configuration contiennent les informations suivantes :
* *BID* du switch source
* *BID* du *Root Bridge* actuel

#### Fonctionnement de STP

1. Election d'un *Root Bridge* (*RB*)
    * king of the switches
2. Sur tous les autres switches, élection d'un *Root Port* (*RP*)
    * pour chacun des switches, c'est le port qui a le chemin le plus court vers le *RB*
    * afin de déterminer quel est le port qui a le chemin le plus court, cela se fait en fonction de la priorité et la vitesse des liens (10M/s, 100M/s, etc)
3. Négotiation entre les switches pour déterminer quels ports passer dans un état *Forwarding* (*FWD*)
    * cela se fait en fonction de la priorité des liens
4. Tous les autres ports sont passés dans un état "Blocking" (*BLK*)

**Cet algorithme open-source garantit la création d'une topologie *loop-free*** (sans [boucle topologique L2](#concept-de-boucle-topologique)
) et ainsi éviter les soucis qui y sont liés, [vus dans la section précédente](#concept-de-boucle-topologique). 

#### STP algorithms

Il existe plusieurs variantes du protocoles STP en particulier RSTP, lui aussi open-source, qui permet d'accéder à des temps de convergence bien plus courts.  

Le temps de convergence correspond au temps que met l'infrastructure de switches à se mettre d'accord sur une nouvelle topologie STP. 

Cisco a de son côté créé deux protocoles basés respectivement sur STP et RSTP mais intégrant une gestion des VLANs. On dit que ces protocoles (PVST+, PVRST+) sont *VLAN aware*.

## VLAN

### Besoin

Il est courant d'avoir besoin d'isoler certains équipements les uns des autres sur le réseau. Par exemple, on souhaite souvent isoler les utilisateurs finaux du parc de serveur, afin d'éviter tout problème résultant d'une interaction non-gérée entre les deux parties (*i see u crazy little hacker*).

Afin de maîtriser complètement les flux réseau et les isoler le plus tôt possible lorsque c'est nécessaire, on pourrait penser au [firewall](/memo/lexique.md#pare-feu-ou-firewall). Sauf que traditionnellement, un [firewall](/memo/lexique.md#pare-feu-ou-firewall) fonctionne essentiellement sur deux critères : [IP](/memo/lexique.md#ip-internet-protocol-v4) et [ports (TCP/UDP)](/memo/lexique.md#couche-4-du-modèle-osi) : c'est trop tard (respectivement niveau 3 et 4).

En gérant ça avec des switches, on pourrait isoler ça niveau 2 voire niveau 1 : 
```
+------+       +-----+                +-----+         +---+
| SRV2 +-------+ SW1 |                | SW2 +---------+PC2|
+------+       +--+--+                +--+--+         +---+
                  |                      |
                  |                      |
                  |                      |
               +--+---+                +-+-+
               | SRV1 |                |PC1|
               +------+                +---+
```

Ici les switches permettent une isolation totale des `PC`s par rapport aux `SRV`s. 

Sauf que :
* un switch, ça coûte cher
* plus il y a d'équipements, plus il y a de complexité, plus c'est difficile à maintenir

**VLANs à la rescousse.**

### Concept

Les VLANs permettent la mise en place de l'infra de la section précédente, avec (quasiment) le même niveau d'isolation, mais **un unique switch**.

```
                +---+
                |PC2|
                +-+-+
                  |
                  |
                  |20
+------+     10+-----+        +---+
| SRV2 +-------+ SW1 +--------+PC1|
+------+       +-----+20      +---+
                10|
                  |
                  |
               +--+---+
               | SRV1 |
               +------+
```

Afin d'expliquer le fonctionnement, prenons le cas où `PC1` essaie de joindre `SRV1` : 
1. `PC1` envoie une trame avec pour destination `SRV1`
2. `SW1` récupère cette trame et voit qu'elle vient d'un de ses ports taggés "VLAN20" (ou plus simplement, on va le noter `20`)
3. `SW1` modifie la trame de `PC1` et y ajoute un tag `20`
4. `SW1` achemine la trame vers son interface de sortie, celle qui pointe vers `SRV1`
5. `SW1` remarque que la trame est taggée `20` alors que le port pour joindre `SRV1` est taggé `10`
6. **`SW1` va jeter la trame car le tag de la trame et le tag du port de destination sont différents**
7. la trame envoyée par `PC1` en 1. n'arrivera jamais à destination

### Cisco : ports *access* et *trunk*

Sur du matériel Cisco (entre autres) il existe deux types de ports pour un switch qui met en place des VLANs : 
* ***access*** : les ports en mode *access* sont les ports utilisés par les end devices. Un seul VLAN pourra circuler sur ce port
* ***trunk*** : on trouve souvent les ports *trunk* entre deux équipements réseau. Un lien *trunk* permet de faire transiter plusieurs VLANs

```
          A        T           T       A
+----+    10+-----+10,20   10,20+-----+10      +----+
|SRV1+------+ SW1 +-------------+ SW2 +--------+SRV2|
+----+      +-----+             +-----+        +----+
              A|                   |A
             10|                   |20
               |                   |
               |                   |
             +-+-+               +-+-+
             |PC1|               |PC2|
             +---+               +---+
```

### VLAN natif

La notion de *native VLAN* est nécessaire dès lors qu'il est nécessaire de traiter des trames non taggées sur un port trunk. Certains protocoles comme CDP utilise par défaut des trame snon-taggées pour fonctionner. 

Cela dit cela, une faible configuration rend vulnérable le concept de VLAN (*VLAN hopping*) bien qu'il existe des contre-mesures (tagging du native *VLAN* par exemple).

### Bonnes pratiques

* le numéro de VLAN est obligatoire, c'est son ID. Le nom est optionnel, mais c'est une bonne pratique de **nommer tous les VLANs** :
  * un nom explicite
  * utiliser le même nom pour un VLAN donné sur tous les switches (*eg.* le VLAN20 s'appelle "admin" sur tous les switches)
* associer à chaque VLAN une plage d'adresse IP 
  * voire intégrer le numéro du VLAN dans l'adresse de réseau
  * *eg.* `10.1.10.0/24` pour les hôtes du VLAN10 et `10.1.20.0/24` pour les hôtes du VLAN20

## LACP

LACP pour *Link Aggregation Control Protocol* est un protocole standard qui permet d'agréger plusieurs ports (physiques) d'un équipement en un unique port virtuel. **On appelle ce port virtuel un *port-channel*** (avec *LACP* ou un autre protocole d'agrégation).

LACP permet d'accéder à plusieurs fonctionnalités :
* augmenter la bande passante d'un lien donné
* répartition de charges (*loadbalancing*) sur les différents ports du *port-channel*
* tolérance de pannes (*failover*) : tant qu'un lien physique est UP, le *port-channel* sera fonctionnel

Contraintes : 
* les liens doivent être de la même vitesse (100M/s par exemple)
* utiliser un mode full-duplex des deux côtés du LACP

LACP est un protocole qui inclut une négociation entre les deux équipements reliés. Il est donc nécessaire de le configurer à l'identique sur les deux équipements reliés. 